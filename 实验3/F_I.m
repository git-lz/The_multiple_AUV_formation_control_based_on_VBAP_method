function [f_I] = F_I( x,alpha_I,d0 )
%F_I 此处显示有关此函数的摘要
for i=1:3
    for j=1:3
        % 计算beta
        beta(i,j)=Beta(x(i,j),90,100);
        % 计算f_h_c
        f_I(i,j)=alpha_I*beta(i,j)*(x(i,j)-d0^2/x(i,j));
    end
end
end

