function [ Dist ] = Distance( x,y,dimension )
%DISTANCE 求两点间x和y的距离
%dimension是维数，二维或三维

if dimension==2       %二维空间
    Dist=sqrt((x(1)-y(1))^2+(x(2)-y(2))^2);
end
if dimension==3       %三维空间
    Dist=sqrt((x(1)-y(1))^2+(x(2)-y(2))^2+(x(3)-y(3))^2);
end
end

